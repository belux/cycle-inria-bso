 https://gitlab.inria.fr/belux/cycle-inria-bso/-/edit/master/README.md

# cycle-inria-bso

Élaboration d'un texte pour réfléchir et soutenir l'utilisation du vélo dans l'établissement Inria Bordeaux Sud-Ouest.  




## Introduction

Ce document fait suite aux remarques sur le stationnement des vélos par un certains nombre d'agents Inria Bordeaux Sud-Ouest (bso).  
Il vise à faire un constat sur l'existant et à rassembler un argumentaire afin d'expliquer le points de vue d'une partie du personnel Inria.  


## Problématique

Plusieurs utilisateurs des parking vélo (intérieur et extérieurs) ont remonté divers désagréments de ces parking.  
Depuis un certains temps ces remarques essayent remonter de façon sporadique et de manière plus ou moins formelle pour que l'usage de cet espace change.  

Les différentes remarques remontées par le personnel :  
 - Pas assez d'espace entre les vélos.  
 - Pas assez de place pour les vélos  
 - Rapport de l'espace alloué aux voitures face aux vélos inéquitable  
 - Aucun dispositif pour l'équipement cycliste  
 - Parking (intérieur/extérieur) vélo mal adapté  
 - Porte du parking vélo dangereuse  


### Espace entre vélo
les parking cycles d'Inria sont constitués de dispositifs pince roue. Ceux ci sont très rapprochés, ce qui pose plusieurs soucis. Cet espace trop étroit ne permet pas de remplir tout les emplacements. Ainsi le nombre d'emplacement pince roue ne correspond pas à la capacité des parkings cycle. Les vélos ne peuvent être disposés qu'au mieux par les utilisateurs. De plus cet espace réduit force les agents à se contorsionnés après avoir garé et/ou attaché leur vélo. L'espacement actuel entre deux places est de ***TODO : mesurer l'espacement*** pour les places extérieures et ***TODO : mesurer l'espacement*** pour les places intérieures contre les 60 à 65 cm recommandé par divers organismes. ***TODO : prendre les chiffres Adem ?*** 

### Nombre de place vélo
Comme évoqué plus haut, le nombre de places dont dispose l'Inria n'est pas celui de pinces roue présent. J'estime qu'on ne peut pas faire mieux que 2 vélos pour 3 pince roue (pour le parking intérieur) dans des condition optimal de rangement. Je suis preneur de toute démonstration avec un taux de remplissage plus élevé (dans des conditions de non détérioration des cycles). On arrive ici à un total de 50 places au mieux : 26 parking intérieur, 12 extérieur avant, 12 extérieur arrière.  
Cette année, durant la période décembre 209 à mi-mars 2020, j'ai pu constater un nombre de vélo conséquent et ce quelle que soit la météo. On a une baisse lors de certains jours de forte pluie, mais par exemple un jour humide comme le 12 mars 2019 voit une forte utilisation de ses places cycles (21/26 intérieur, 4/12 extérieur avant, 11/12 arrière).  
***TODO : L'an dernier un comptage du nombre de vélo par les services généraux + résultats ***  
Dans se contexte le manque de places se fait sentir notamment car les places sont serrées. On peut supposer qu'a l'avenir, notamment dans des periodes de beau temps, ce nombre sera insuffisant. Ce manque de palace limite aussi l'attractivité du vélo pour de nouvelles personnes qui voudraient changer de moyen de locomotion ou les nouveaux arrivants.  

### Place prise par les voitures
La place au sol prise par un emplacement de vélo correct est bien inférieure à la place nécessaire par une voiture. Et puisque les voitures sont majoritairement utilisées en tant auto-soliste, un automobiliste utilise la place nécessaire pour garer au moins  8 cycles. Dans certaines configuration ce rapport est encore plus défavorable. Il en ressort un sentiment d'être lésé quand les places viennent à manquer.  

### Aucun dispositif pour l'équipement cycliste
L'Inria ne dispose que de peu d'équipement pour faciliter les allers retours en vélo. En positif nous disposons de douches ce qui facilite la décision de faire des trajets à partir d'une certaine distance. Par contre aucun dispositif de stockage pour les casques, lampes, sacoches et autres accessoires et vêtement cyclistes n'existent. Tout le monde sait bien qu'on ne peut pas enlever l'intégralité des accessoires d'un vélo et les remonter dans nos bureaux. C'est d'autant plus pénible pour les affaires de pluie lorsqu'elles sont trempées. Un endroit où les sécher saurait être appréciable. En effet un cycliste est bien plus mouillé qu'un automobiliste ou qu'un piéton avec parapluie, par temps de pluie. Pourtant on voit des messages des services généraux qui incitent les gens à ne pas laisser leurs accessoires sur les vélos. Le respect de ces consignes ne peut pas être envisager sans que l'Inria ne mette à disposition le matériel adéquat.  

### Dispositifs de parking non adaptés
Le parc de stationnement de cycles Inria est constitué uniquement de pinces roue. Ces dispositifs ne sont pas adaptés au parking vélo. De nombreuse raisons ont conduit toutes les associations et pouvoir publique qui traitent de la mobilité à vélo à préconiser les arceaux et bannir les pinces roues.  

### Porte du parking vélo dangereuse
La porte du parking vélo est également remise en cause. Elle est en effet lourde et très compliquée à manipuler tout en tenant un vélo. Il y a souvent des chutes de vélo ou même des personnes qui tombent du à cette difficulté de manipulation.  



## Réponse possibles

Vis à vis de ses remarques plusieurs solutions peuvent être mises en place. La liste suivante de proposition n'est pas exhaustive. Elle est faite avec les idées porté par des non spécialistes mais nous semblent cohérentes.  
Pour chacune d'elles nous proposons une différents problème qu'elle résout. Ces problèmes sont classé dans 4 catégories : Pratique (P), Matériel (M), Sécurité (S), Éthique (E), Issue (I) (***TODO anglicisme, mais je n'ai pas mieux***)  
Liste de solutions :  

### Installation d'un deuxième garage à vélo sous-terrain.
 - (P, S) : On ne passe pas par la porte peu pratique
 - peu de frais
 - (E) impacte minimal (à partir d'une place de parking pour 10 places vélos)
 - voir propositions de Gaël
 - (P, M, S) évidement avec des arceaux

### Remplacement des pinces roue par des arceaux
  -
### Installation de patères
### Installation de casiers

## Avantages / Évolutions / Contraintes


Différents emménagements pourrait favoriser les bonnes conditions de stationnement des cyclistes.


### Pratique :
 - Arceaux versus Pince roue (AvP) : plus facile d’attacher l'anti vol  
 - plus d'espacement entre les vélo : confort de déplacement  
 - espacement : les Pinces ne sont de toutes façon pas utilisées à 100% (par limitation physique). On ne peut pas dire qu'on gagne en nombre de place en rapprochant les pinces roues (parce que ces places\
 sont inutilisables).  
 - patères pour vêtements de pluie : On a pas à transporter les veste / pantalons de pluie à travers le bâtiment.  

### Matériel :
 - espacement : moins de risque de détériorer les vélos des voisins  
 - AvP : moins de contrainte sur les rayon. Un rayon n'a pas vocation à supporté le poids entier du vélo !  
 - AvP : moins de contrainte sur les garde boue. Je ne peux pas garer mon vélo que parce que j'ai une béquille, mon garde boue m’empêche de coincer ma roue (et je ne souhaites pas le faire).  
 - espacement : on salis nos vêtement en se frottant au vélos trop serrés.  
 - plus de place en intérieur : protection contre la pluie (du vélo et du cycliste)  

### Sécurité :
 - AvP : plus de sécurité au niveau du vol, meilleur attache, permet de mettre les deux roues.  
 - espacement : moins de risque de d’accidents de travail. Avec les vélos si serrés, et le système de pince-roues inadapté aux antivols, on est contraint à faire des acrobaties dangereuses pour attacher\
 et détacher son vélo, avec risque de blessure, faux mouvement, mal de dos, ...  
 - patères : moins d'humidité dans le bâtiment ? moins d'eau dans les couloirs ? (peut être pas dans la bonne section "sécurité")  

### Éthique :
 - promotion de méthode de déplacement douce (c'est pas dans certains axes ?)  
 - les vélo prennent moins de place, donc le nombre de personne à qui profite l'espace est plus élevé.  
 - c'est en mettant en place les infrastructures qu'on favorise la pratique.  

### Problèmes possibles :
 - patères : matériel personnel non protégé (je le note, mais je ne crois pas au vol à l'intérieur du bâtiment)  
 - casiers : problème de place ? (je pense que la superficie d'un casier + vélo reste inférieure à celle d'une voiture.)  
 - casiers : détournement d'usage : utilisation de façon abusive par quelques uns (à voir si c'est dérangent si ils servent...)  
 - casiers : nécessité de pouvoir identifier qui utilise les casiers ? (denrées périssables, éléments dangereux...) -> utilisation d'une identité sur le cadenas ? attribution des casiers (via demande) ?  


## Conclusion

Politique d'encouragement de la mobilité douce dans un contexte ou on veut prendre en compte les enjeux environnementaux (***TODO : check COP ? ***)  
Effet rebond : pour un fois on peut l'utiliser dans le bon sens. Augmenter les infrastructures pour favoriser les usages.  
Agir à son niveau, responsabilité de l'entreprise. Les pistes cyclables pour les communauté d'aglo, le parking pour Inria.  







## Annexes :

Rapport du 12 mars 2020 :  
Au jour d'aujourd'hui:  
Jeudi 12 mars, 15h00, temps couvert, peu de pluie mais très humide.  
 
arrière : 2x6 places, 11 vélos  
avant ext : 12 places, 4 vélos  
avant int : 49 pinces, 26 places, 21 vélos.  

Parking voiture : 11 places plus 3 handicapées et 3 services.  





